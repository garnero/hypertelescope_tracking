This is a proof of concept to track Vega star using:
* a MAKO G032B camera
* a set of image "homemade" using a more or less 1.5mm pinhole located between 5.7 and 5.9 cm of the camera objective ... 
* ... with an objective unscreewed a little bit to match characteristics of the hypertelescope...

So, yes, this a lot of "uncertainty"/inacurracy...

Please note that a reference image (with the same settings) has been captured and is located under "reference" directory.
The square size is 15mmx15mm.


The program implemented by "src/tracking.cpp" source file takes an image reference in test/test_mako/move(*) and try to compare
the position (x,y) of the simulated "vega" start with all the pictures located in this directory.
It should be noted that if the algorithm is correct, it will not be a big deal to adapt it in order to deal with images received
on TCP/IP using the StorageService.

(*) : do not forget to untar test/test_mako.tar.gz before launching hypertelescope_tracking
