#include <memory>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>

#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/filesystem.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

struct Shift {
 Shift() = default;
 Shift(float x_, float y_) : 
   _x(x_),
   _y(y_)
  {}
 ~Shift() = default; 
 void display(const std::string& filename_) 
 {
   BOOST_LOG_TRIVIAL(info) << "Shift observed for " << filename_ << "  =>  X: " << _x << " - Y: " << _y;
 }
 float _x;
 float _y;
};

class VegaImage {
public:
  VegaImage(const std::string& file_) :
    _image(nullptr),
    _preprocessed(nullptr)
  {
#if 0 
  // It would have been too simple to do that... reading raw images is not supported by OPENCV 
  cv::Mat image(658, 492, CV_8UC1);

  image = imread(file_.c_str(),
                 cv::IMREAD_GRAYSCALE);
#endif
    FILE *fp = fopen(file_.c_str(),"rb");

    char imagedata[sizeof(char) * _frame_size];

    fread(imagedata, sizeof(char), _frame_size, fp);

    _image = std::make_shared<cv::Mat>();

    _image->create(_image_height, _image_width, CV_8UC1);

    memcpy(_image->data, imagedata, _frame_size);

    fclose(fp);
  }
  ~VegaImage() = default;
  void showReference(const std::string& windowName_)
  { 
    cv::namedWindow(windowName_, cv::WINDOW_AUTOSIZE );
    imshow(windowName_, *_image);
  }
  void showReferenceBlob(const std::string& windowName_)
  { 
    cv::Mat im_with_keypoints;
    drawKeypoints(*_preprocessed, _blobs, im_with_keypoints, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
    cv::namedWindow(windowName_, cv::WINDOW_AUTOSIZE);
    imshow(windowName_, im_with_keypoints);
  }
  void preProcess(double threshold_)
  {
    // Just keep what is really bright.
    cv::Mat threshold;
    (void)cv::threshold(*_image, threshold, threshold_, 255.0, cv::THRESH_BINARY );

    // Try to keep only "large" objects
    cv::Mat element = getStructuringElement(cv::MORPH_RECT,cv::Size(5,5));
    cv::Mat eroded;
    cv::Mat dilated;
    erode(threshold, eroded, element);
    _preprocessed = std::make_shared<cv::Mat>();
    dilate(eroded, *_preprocessed, element);
#if 0
    cv::namedWindow("Preprocessed", cv::WINDOW_AUTOSIZE );
    imshow("Preprocessed", *_preprocessed);
#endif
  }
  size_t detectBlobs()
  {
    cv::SimpleBlobDetector::Params params;

    // Change thresholds
    params.minThreshold = 1;
    params.maxThreshold = 255;

    params.filterByColor = true;
    params.blobColor = 255;
    params.filterByArea = false;
    params.filterByCircularity = false;
    params.filterByConvexity = false;
    params.filterByInertia = false;

    // Detect blobs
    cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
    detector->detect(*_preprocessed, _blobs);

    // Retrieve the blob with the largest area: we assume it is Vega...
    float largestArea=0.0;
    for(auto keypoint: _blobs) {
      auto currentSize = keypoint.size;
      if (currentSize>=largestArea) {
        _vega = keypoint;
        largestArea = currentSize; 
      } 
    }
    
    return _blobs.size();
  }
  const cv::KeyPoint& getKeyPoint() const 
  {
    return _vega;
  }
  void computeShift(const std::shared_ptr<VegaImage>& imageInit, Shift& shift)
  {
    const cv::KeyPoint& ref = imageInit->getKeyPoint();
    shift._x = _vega.pt.x - ref.pt.x;   
    shift._y = _vega.pt.y - ref.pt.y;   
  }
private:
  static const int _image_width = 658;
  static const int _image_height = 492;
  static const int _frame_size = _image_width * _image_height;
  std::shared_ptr<cv::Mat>  _image;
  std::shared_ptr<cv::Mat>  _preprocessed;
  std::vector<cv::KeyPoint> _blobs;
  cv::KeyPoint              _vega;
};

namespace po = boost::program_options;
namespace logging = boost::log;
namespace fs = boost::filesystem;

static const double configThreshold = 200.0; // Threshold will have to be tuned... Probably a function of camera gain...

int main() {

  logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::trace);

  BOOST_LOG_TRIVIAL(info) << "Starting";

  // Mako resolution is 658x492
  // We deal with raw image so 8 bits depth, single channel
  
  std::shared_ptr<VegaImage> imageInit = std::make_shared<VegaImage>("../test/test_mako/move/image_20160820_193512_0.raw"); 

  imageInit->showReference("Reference");
  imageInit->preProcess(configThreshold);
  auto nbDetectedBlobs = imageInit->detectBlobs();
  if (nbDetectedBlobs != 1) {
    if (nbDetectedBlobs == 0) {
      BOOST_LOG_TRIVIAL(fatal) << "No star found. Exiting";
      exit(EXIT_FAILURE);
    }
    else {
      BOOST_LOG_TRIVIAL(warning) << "Multiple stars found. Reference is the one with the largest surface.";
    }
  }
  imageInit->showReferenceBlob("Blob");

  // Compare images with the references as if it was a movie, that is in the order of the snapshot...
  // The name of the file is a timestamp, so it it sufficient to sort the file names...
  fs::path p ("../test/test_mako/move");
  fs::directory_iterator end_itr;
  std::vector<std::string> fileImages;

  // cycle through the directory
  for (fs::directory_iterator itr(p); itr != end_itr; ++itr)
  {
    if (is_regular_file(itr->path())) {
      fileImages.push_back(itr->path().string());
    }
  }
  std::sort(fileImages.begin(), fileImages.end());

  for(auto name: fileImages) {
    auto image = std::make_shared<VegaImage>(name);
    image->preProcess(configThreshold);
    auto nbDetectedBlobs = image->detectBlobs();
    if (nbDetectedBlobs != 1) {
      if (nbDetectedBlobs == 0) {
        BOOST_LOG_TRIVIAL(warning) << "No star found (" << name << "). Skipping";
        continue;
      }
      else {
        BOOST_LOG_TRIVIAL(warning) << "Multiple stars found. Reference is the one with the largest surface.";
      }
    }
    Shift shift;
    image->computeShift(imageInit,shift);
    shift.display(name);
  }
 
  cv::waitKey(0); 

  return 0;
}
